import sys
import logging
from time import sleep
import signal

from gpiozero import LED, Button

import Adafruit_DHT

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

class Firebase:
    project_id = u'the-reeds-chicken-coop'
    db = None
    config = None
    door_ref = None
    env_ref = None
    door_names = { u'main', u'sliding', u'nesting' }
    open_doors = None

    def __init__(self, *project_id):
        cred = credentials.ApplicationDefault()
        if project_id:
            self.project_id = project_id

        logging.info(u'Initializing Firebase')
        firebase_admin.initialize_app(cred, {
            u'projectId': self.project_id,
        })

        self.db = firestore.client()

        self.doors_ref = self.db.collection(u'coop').document(u'open_doors')
        self.env_ref = self.db.collection(u'coop').document(u'environment')

    # Gets configuration, either from cached or from database
    def get_config(self):
        if not self.config:
            config_ref = self.db.collection(u'coop').document(u'configuration')
            self.config = config_ref.get().to_dict()

        return self.config

    def get_open_doors(self):
        self.open_doors = self.doors_ref.get().to_dict()
        return self.open_doors 

    def set_open_doors(self, open_doors):
        self.doors_ref.set(open_doors)

    def set_environment(self, humidity, temperature):
        self.env_ref.set({ u'humidity': humidity, u'temperature': temperature })

class Coop:
    firebase = None
    config = None

    env_timer = 0
    env_timer_interval = 120


    led_pin = 4
    door_main_pin = 14
    door_slide_pin = 15
    door_nest_pin = 17
    env_pin = 2

    led = None

    doors = { u'main': None, u'sliding': None, u'nesting': None }

    env_sensor = Adafruit_DHT.DHT22

    open_doors = None
    humidity = None
    temperature = None

    make_updates = False

    def init(self):
        self.setup()
        self.loop()

    def setup(self):
        logging.info(u'initialize electronics')
        self.led = LED(self.led_pin)
        self.doors[u'main'] = Button(self.door_main_pin)
        self.doors[u'sliding'] = Button(self.door_slide_pin)
        self.doors[u'nesting'] = Button(self.door_nest_pin)

        logging.info(u'Door electronics: {}'.format(self.doors))
        
        self.firebase = Firebase()
        self.config = self.firebase.get_config()

        self.open_doors = self.firebase.get_open_doors()

        logging.info(u'Config: {}'.format(self.config))
        logging.info(u'DB doors {}'.format(self.open_doors))

    def loop(self):
        logging.info(u'Ready to watch for the door')
        for _ in range(1400):
            signal.alarm(1200) 

            self.check_doors()
            self.check_environment()

            sleep(0.5)

    def check_doors(self):
        # Loop through Doors
        for key, door in self.doors.items():
            logging.debug(u'Checking door {}, {}, {}'.format(key, door.value,
                                                        self.open_doors[key]))
            door_open = door.value
            if door_open != self.open_doors[key]:
                self.make_updates = True
                self.open_doors[key] = door_open

        if self.make_updates:
            logging.info(u'Changes were made')
            self.set_doors()
            self.set_led()
            self.make_updates = False

    def set_doors(self):
        logging.info(u'Setting open doors in DB: {}'.format(self.open_doors))
        self.firebase.set_open_doors(self.open_doors)

    def check_environment(self):
        self.env_timer += 1

        humidity, temperature = Adafruit_DHT.read(self.env_sensor, self.env_pin)
        if humidity is not None and temperature is not None:
            logging.debug(u'Temp={0:0.1f}°C  Humidity={1:0.1f}%'
                    .format(temperature, humidity))

            self.set_environment(humidity, temperature)

    def set_environment(self, humidity, temperature):
        if self.env_timer > self.env_timer_interval:
            self.env_timer = 0

            if humidity != self.humidity and temperature != self.temperature:
                logging.debug(u'Updating environment... Temp={0:0.1f}*C  Humidity={1:0.1f}%'
                                    .format(self.temperature, self.humidity))
                self.firebase.set_environment(humidity, temperature)

        self.humidity = humidity
        self.temperature = temperature

    def set_led(self):
        # Loop through doors to check how to set LED
        led_on = False
        for key, door in self.open_doors.items():
            logging.debug(u'This Door: {}'.format(door))
            if door:
                led_on = True

        self.led.value = led_on


if __name__ == '__main__':
    # If we get an argument, then log to that file, otherwise use stderr
    logging_config = {
        u'format': '%(asctime)s %(message)s',
        u'level': logging.INFO

    }
    if len(sys.argv) > 2:
        logging_config[u'filename'] = sys.argv[2]

    log_levels = {
        u'DEBUG': logging.DEBUG,
        u'INFO': logging.INFO,
        u'WARNING': logging.WARNING,
        u'ERROR': logging.ERROR,
        u'CRITICAL': logging.CRITICAL
    }

    if len(sys.argv) > 1:
        if sys.argv[1] in log_levels:
            logging_config[u'level'] = sys.argv[1]

    logging.basicConfig(**logging_config)

    coop = Coop()
    coop.init()
