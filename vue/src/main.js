// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueLogger from 'vuejs-logger';
import Vuetify from 'vuetify';
import colors from 'vuetify/es5/util/colors';
import 'vuetify/dist/vuetify.min.css';
import App from './App';
import router from './router';
import store from './store';

const loggerConfig = {
  logLevel: process.env.LOG_LEVEL,
  stringifyArguments: true,
};

Vue.config.productionTip = false;
Vue.use(VueLogger, loggerConfig);

Vue.use(Vuetify, {
  theme: {
    primary: colors.lightGreen.darken1,
    secondary: colors.lightGreen.lighten1,
    accent: colors.lightGreen.accent4,
  },
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
});
