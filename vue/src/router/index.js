import Vue from 'vue';
import Router from 'vue-router';
import Coop from '@/components/Coop';
import Login from '@/components/Login';
import Configuration from '@/components/Configuration';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Coop',
      component: Coop,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/config',
      name: 'Configuration',
      component: Configuration,
    },
  ],
});
