import Vue from 'vue';
import Vuex from 'vuex';
import Firebase from 'firebase';
import 'firebase/firestore';
import config from '../firebase-config';
import users from './users';
import configuration from './configuration';
import openDoors from './open-doors';

Firebase.initializeApp(config);

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    db: Firebase.firestore(),
  },
  modules: {
    users,
    configuration,
    openDoors,
  },
});
