const mutations = {
  setOpenDoors(state, openDoors) {
    state.openDoors = openDoors;
  },
};

const actions = {
  async get({ commit, rootState }) {
    const openDoorRef = rootState.db.collection('coop').doc('open_doors');
    const openDoors = await openDoorRef.get();

    commit('setOpenDoors', { openDoors });
  },
};

const state = {
  openDoors: {
    main: false,
    sliding: false,
    nesting: false,
  },
};

export default {
  namespaced: true, state, mutations, actions,
};
