const mutations = {
  setUser(state, value) {
    state.currentUser = value;
  },
};

const actions = {};

const state = {
  currentUser: null,
};

export default {
  namespaced: true, state, mutations, actions,
};
